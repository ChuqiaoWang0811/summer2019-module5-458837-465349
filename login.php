<?php
require 'database.php';

//use a prepared statement
$stmt = $mysqli->prepare("SELECT username, hashed_password FROM users WHERE username=?");

  // Bind the parameter
  $stmt->bind_param('s', $user);
  $user = $_POST['username'];
  $stmt->execute();
  
  $stmt->bind_result($cnt, $user_id, $pwd_hash);
  $stmt->fetch();
  $inputpassword = $_POST['password'];

  if($cnt == 1 && password_verify($inputpassword, $pwd_hash)){
    // Login succeeded!
    session_start();
    $_SESSION['token'] = bin2hex(random_bytes(32));
    $_SESSION['user_id'] = $user_id;
    header("Location: homepage.php");

} else{
    header("Location: login.php");
}

?>