<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CALENDAR</title>
    </head>
    <body>
         <?php 
            session_start();
            $user_id = $_SESSION['user_id']; //creates a session username using the inputted username
            if ($user_id == null) {
                //question: unregistered users could still view the homepage (by rubrics)
                header("Location: loginpage.php");
            }
        ?>
        <h1>CALENDAR</h1>
        <!--directories to logout, write story, user profile, and change password-->
        <form action="logout.php" method="POST">
            <input type="submit" name="logoutsubmit" value="Log out"/>
        </form>
        <form action="userprofile.php" method="POST">
            <input type="submit" name="userprofilesubmit" value="View My Profile"/>
        </form>
        <form action="changepassword.php" method="POST">
            Enter New Password: <input type="text" name="newpassword">
            <input type="submit" name="changepasswordsubmit" value="Change Password"/>
        </form>
        <hr>
              
    </body>
</html>
