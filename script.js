let d = new Date();
let today = d.getDate();
let month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
let month_idx = d.getMonth();
let currentMonth_idx = d.getMonth();
let year = d.getFullYear();
let curYear = d.getFullYear();


document.addEventListener("DOMContentLoaded", currentMonth, false);
document.getElementById("pre_Month_click").addEventListener("click", preMonth, false);
document.getElementById("next_Month_click").addEventListener("click", nextMonth, false);
//document.getElementsByClassName("curMonthDay").addEventListener("click", test, false);

function test(){
	alert("sdf");
}

function callModel() {
  var modal = document.getElementById("myModal");
  var span = document.getElementsByClassName("close")[0];
  modal.style.display = "block";
  span.onclick = function() {
  modal.style.display = "none";
  }
  // When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
}

function startListen(){
  let classname = document.getElementsByClassName("curMonthDay");
  for (var i = 0; i < classname.length; i++) {
      classname[i].addEventListener('click', test, false);
  }
  document.getElementById("today").addEventListener("click", test, false);
}

// click next mon
function nextMonth(){
  clear();
  if(month_idx == 11){
    year = year + 1;
    month_idx = 0;
  } else{
    month_idx = month_idx + 1;
  }
  loadCurMonth();
}

//click pre mon
function preMonth(){
  clear();
  if(month_idx == 0){
    year = year - 1;
    month_idx = 11;
  } else{
    month_idx = month_idx - 1;
  }
  loadCurMonth();
}

function currentMonth(){
    clear();
    loadCurMonth();
}

// get the current month after click or load in
function loadCurMonth() {
  let first_date = month_name[month_idx] + " " + 1 + " " + year;
  //July 1 2019
  let tmp = new Date(first_date).toDateString();
  //Mon July 01 2019 ...
  let first_day = tmp.substring(0, 3);    //Mon
  let day_name = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
  let first_day_num = day_name.indexOf(first_day);   //1
  let days = new Date(year, month_idx+1, 0).getDate();    //31
  let pre_Month_days = new Date(year, month_idx, 0).getDate();
  // get_calendar, build all the date nodes
  let calendar = get_calendar(first_day_num, days, today, pre_Month_days, month_idx, currentMonth_idx);
  let node = document.createTextNode(" " + month_name[month_idx]+ " " + year + " ");
  document.getElementById("calendar-month-year").appendChild(node);
  //document.getElementById("calendar-month-year").appendChild(node);
  //document.getElementById("calendar-month-year").innerHTML = month_name[month]+" "+year;
  document.getElementById("calendar-dates").appendChild(calendar);
  startListen();
}



// load in all the date nodes into calendar
function get_calendar(first_day_num, days, today, pre_Month_days, month_idx, currentMonth_idx){
    let table = document.createElement('table');
    let tr = document.createElement('tr');
    // <th> S M T W T F S
    for(let c=0; c<=6; c++){
        let th = document.createElement('th');
        let node = document.createTextNode("SMTWTFS"[c]);
        th.appendChild(node);
        tr.appendChild(th);
    }
    table.appendChild(tr);

    //<td> 1st row
    tr = document.createElement('tr');
    let c;
    let pre_Month_day = pre_Month_days - first_day_num + 1;
    for(c=0; c<=6; c++){
        if(c == first_day_num){
            break;
        }
        let td = document.createElement('td');
        td.setAttribute("class", "last_month");
        let node = document.createTextNode(pre_Month_day);
        td.appendChild(node);
        pre_Month_day ++;
        tr.appendChild(td);
    }

    let count = 1;
    for(; c<=6; c++){
      if(count == today && month_idx == currentMonth_idx && curYear == year){
        let td = document.createElement('td');
        td.setAttribute("id", "today");
        let node = document.createTextNode(count);
        td.appendChild(node);
        count++;
        tr.appendChild(td);
        continue;
      }
        let td = document.createElement('td');
        td.setAttribute("class", "curMonthDay");
        let node = document.createTextNode(count);
        td.appendChild(node);
        count++;
        tr.appendChild(td);
    }
    table.appendChild(tr);

    //rest of the date rows
    let newDay = 1;
    for(let r=3; r<=7; r++){
        tr = document.createElement('tr');
        for(let c=0; c<=6; c++){
            if(count > days){
                count ++;
                let td = document.createElement('td');
                td.setAttribute("class", "nextMonthDay");
                let node = document.createTextNode(newDay);
                td.appendChild(node);
                newDay++;
                tr.appendChild(td);
                continue;
            }
            if(count == today && month_idx == currentMonth_idx && curYear == year){
              let td = document.createElement('td');
              td.setAttribute("id", "today");
              let node = document.createTextNode(count);
              td.appendChild(node);
              count++;
              tr.appendChild(td);
              continue;
            }
            let td = document.createElement('td');
            td.setAttribute("class", "curMonthDay");
            let node = document.createTextNode(count);
            td.appendChild(node);
            count++;
            tr.appendChild(td);
        }
        table.appendChild(tr);

    }
    return table;
 }



// delete all the nodes
function clear(){
  // delete all childnodes of mm-yyyy
  if(document.getElementById("calendar-month-year").childNodes.length == 0){
    return;
  }else {
    while (document.getElementById("calendar-month-year").firstChild) {
        document.getElementById("calendar-month-year").removeChild(document.getElementById("calendar-month-year").firstChild);
      }
  }
  // delete all childNodes of dates
  if(document.getElementById("calendar-dates").childNodes.length == 0){
    return;
  }else {
    while (document.getElementById("calendar-dates").firstChild) {
        document.getElementById("calendar-dates").removeChild(document.getElementById("calendar-dates").firstChild);
      }
  }

 }

